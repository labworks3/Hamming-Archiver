#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cstdint>

namespace ArgumentParser {
template <typename T>
class Argument {
    public:
        Argument() {};

        void Default(const char* value) {
            values_.push_back(value);
            is_initialized = true;
            AddHelpInformation(std::string(" [default = ") + std::string(value) + "] ");
        }
        
        template <typename U>
        void Default(const U& value) {
            values_.push_back(value);
            is_initialized = true;
            AddHelpInformation(std::string(" [default = ") + std::to_string(value) + "] ");
        }

        T GetValue(int64_t index = 0) const {
            return values_[index];
        }

        void AddValue(const T& value) {
            is_initialized = true;

            if (is_store_value) {
                if (multi_value) {
                    store_values->push_back(value);
                } else {
                    *store_value = value;
                }
            }

            if (values_.size() > 0 && !multi_value) {
                values_.pop_back();
            }
            values_.push_back(value);

            if (values_.size() < min_args_count_) {
                is_initialized = false;
            }
        }

        Argument<T>& StoreValue(T& value) {
            store_value = &value;
            is_store_value = true;
            return *this;
        }

        Argument<T>& StoreValues(std::vector<T>& values) {
            store_values = &values;
            is_store_value = true;
            return *this;
        }

        Argument<T>& MultiValue() {
            multi_value = true;
            AddHelpInformation(" [repeated] ");
            return *this;
        }

        Argument<T>& MultiValue(int64_t min_args_count) {
            multi_value = true;
            min_args_count_ = min_args_count;
            AddHelpInformation(" [repeated min args = " + std::to_string(min_args_count) + "] ");
            return *this;
        }

        Argument<T>& Positional() {
            positional = true;
            return *this;
        }

        int64_t GetNumberArgumnets() {
            return values_.size();
        }

        void SetHelp() {
            is_help = true;
        }

        bool CheckIsHelp() const{
            return is_help;
        }

        bool CheckIsPostional() const {
            return positional;
        }

        bool CheckIsInitialized() const {
            return is_initialized;
        }

        void SetInitialized() {
            is_initialized = true;
        }

        void AddHelpInformation(std::string information) {
            help_description_ += information;
        }

        std::string GetHelpDescription() const{
            return help_description_;
        }
    private:
        std::string help_description_;
        std::vector<T> values_;
        T* store_value = nullptr;
        std::vector<T>* store_values = nullptr;
        uint64_t min_args_count_ = 0;
        bool multi_value = false;
        bool positional = false;
        bool is_help = false;
        bool is_store_value = false;
        bool is_initialized = false;
};

class ArgParser {
    public:
        ArgParser(const char* name);
        bool Parse(const std::vector<std::string>& arguments);

        Argument<std::string>& AddStringArgument(const char* full_name, const char* description = "");
        Argument<std::string>& AddStringArgument(char short_name, const char* full_name, const char* description = "");
        std::string GetStringValue(const char* name, int64_t index = 0) const;
        int64_t StringArgumentGetNumberElements(const char* name);

        Argument<int>& AddIntArgument(const char* full_name, const char* description = "");
        Argument<int>& AddIntArgument(char short_name, const char* full_name, const char* description = "");
        int GetIntValue(const char* name, int64_t index = 0) const;

        Argument<bool>& AddFlag(const char* full_name, const char* description = "");
        Argument<bool>& AddFlag(char short_name, const char* full_name, const char* description = "");
        bool GetFlag(const char* name, int64_t index = 0) const;

        void AddHelp(const char* full_name = "", const char* description = "");
        void AddHelp(char short_name, const char* full_name, const char* description = "");
        bool Help() const;
        std::string HelpDescription() const;
    private:
        enum class types {
            kStringArgument, 
            kIntArgument, 
            kFlagArgument
        };

        void FindPositional(int64_t& pos_string, int64_t& pos_int, int64_t& pos_flag);
        bool CheckNotInitializedArgs();
        bool LongNameHandle(const std::string& argument);
        bool ShortNameHandle(const std::string& Argument);

        std::string name_;
        //"std::pair<int64_t, types>" : int64_t - index in vector
        std::unordered_map<std::string, std::pair<int64_t, types>> indexes_arguments_;
        std::vector<Argument<std::string>> string_arguments_;
        std::vector<Argument<int>> int_arguments_;
        std::vector<Argument<bool>> flag_arguments_;
        bool was_called_help = false;
        std::string help_description_;
};
}