#pragma once
#include <iostream>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "ArgParser.h"


static const int64_t kLongLongSize = sizeof(uint64_t);
static const int64_t kBitsInByte = 8;

std::ifstream::pos_type GetFileSize(std::string filename);
void ReadCodeWord(std::vector<bool>& codeword, uint8_t& byte_in, int8_t& cur_bit_in, std::istream& in, std::size_t& bytes_read, std::size_t filesize, bool coded);
void WriteCodeWord(std::vector<bool>& codeword, std::ostream& out, int8_t& cur_bit_out, uint8_t& byte_out);
void Encode(std::vector<bool>& codeword, std::size_t number_of_redundant_bits);
void Decode(std::vector<bool>& coded_codeword, std::vector<bool>& decoded_codeword, std::size_t number_of_redundant_bits);
void ReadAndWriteCodedFile(std::ofstream& out ,std::string& filename, std::size_t number_of_redundant_bits, std::size_t filesize, const int64_t kInfoLength);
void ReadAndWriteCodedHeader(std::ofstream& out, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::size_t filename_size, std::string& filename, std::size_t coded_filesize, std::size_t filesize);
void ReadDecodeAndWriteHeader(std::ifstream& in, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::size_t& filename_size, std::string& filename, std::size_t& coded_filesize, std::size_t& filesize);
void ReadAndDecodeFile(std::ifstream& in, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::string& filename, std::size_t coded_filesize);

namespace HamArc {
void CreateArchive(ArgumentParser::ArgParser& parser);
void ShowListOfFiles(ArgumentParser::ArgParser& parser);
void ExtractFile(ArgumentParser::ArgParser& parser);
void AppendFile(ArgumentParser::ArgParser& parser);
void DeleteFile(ArgumentParser::ArgParser& parser);
void ConcatenateArchives(ArgumentParser::ArgParser& parser);
}