#include "runtime.h"

ArgumentParser::ArgParser ManageParsing(uint32_t argc, char** &argv) {
    ArgumentParser::ArgParser parser("Parser");
    parser.AddStringArgument('f', "file");
    parser.AddFlag('c', "create");
    parser.AddFlag('l', "list");
    parser.AddFlag('x', "extract");
    parser.AddFlag('a', "append");
    parser.AddFlag('d', "delete");
    parser.AddFlag('A', "concatenate");
    parser.AddIntArgument('l', "length").Default(8);
    parser.AddStringArgument("filename").Positional().MultiValue();
    std::vector<std::string> arguments(argc);

    for (std::size_t i = 0; i < argc; ++i) {
        arguments[i] = std::string(argv[i]);
    }

    parser.Parse(arguments);
    int64_t number_actions_with_file = 0;


    if (parser.GetFlag("create")) {++number_actions_with_file;}
    if (parser.GetFlag("extract")) {++number_actions_with_file;}
    if (parser.GetFlag("delete")) {++number_actions_with_file;}
    if (parser.GetFlag("append")) {++number_actions_with_file;}
    if (parser.GetFlag("concatenate")) {++number_actions_with_file;}
    if (parser.GetFlag("list")) {++number_actions_with_file;}
    if (number_actions_with_file != 1) {
        std::cout << number_actions_with_file << std::endl;
        std::cerr << "Must be only one command";
        std::exit(EXIT_FAILURE);
    }


    return parser;
}

void Runtime(uint32_t argc, char** &argv) {
    ArgumentParser::ArgParser parser = ManageParsing(argc, argv);
    if (parser.GetFlag("create")) {
        HamArc::CreateArchive(parser);
    } else if (parser.GetFlag("append")) {
        HamArc::AppendFile(parser);
    } else if (parser.GetFlag("list")) {
        HamArc::ShowListOfFiles(parser);
    } else if (parser.GetFlag("extract")) {
        HamArc::ExtractFile(parser);
    } else if (parser.GetFlag("delete")) {
        HamArc::DeleteFile(parser);
    } else if (parser.GetFlag("concatenate")) {
        HamArc::ConcatenateArchives(parser);
    }
}