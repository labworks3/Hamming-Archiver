#pragma once
#include "ArgParser.h"
#include "archiver.h"

void Runtime(uint32_t argc, char** &argv);

ArgumentParser::ArgParser ManageParsing(uint32_t argc, char** &argv);