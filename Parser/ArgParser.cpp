#include "ArgParser.h"

using namespace ArgumentParser;

ArgParser::ArgParser(const char* name) {
    name_ = std::string(name);
    help_description_ += std::string(name) + '\n';
}

Argument<std::string>& ArgParser::AddStringArgument(const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<std::string>();
    new_argument.AddHelpInformation("--" + std::string(full_name) + "=<string>, " + description);
    string_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {string_arguments_.size() - 1, types::kStringArgument};
    return string_arguments_.back();
}

Argument<std::string>& ArgParser::AddStringArgument(char short_name, const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<std::string>();
    new_argument.AddHelpInformation("-" + std::string(1, short_name) + ", --" + std::string(full_name) + "=<string>, " + description);
    string_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {string_arguments_.size() - 1, types::kStringArgument};
    indexes_arguments_[std::string(1, short_name)] = {string_arguments_.size() - 1, types::kStringArgument};
    return string_arguments_.back();
}

std::string ArgParser::GetStringValue(const char* name, int64_t index /*=0*/) const {
    return string_arguments_[indexes_arguments_.at(std::string(name)).first].GetValue(index);
}

Argument<int>& ArgParser::AddIntArgument(const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<int>();
    new_argument.AddHelpInformation("--" + std::string(full_name) + "=<int>, " + description);
    int_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {int_arguments_.size() - 1, types::kIntArgument};
    return int_arguments_.back();
}

Argument<int>& ArgParser::AddIntArgument(char short_name, const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<int>();
    new_argument.AddHelpInformation("-" + std::string(1, short_name) + ", --" + std::string(full_name) + "=<int>, " + description);
    int_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {int_arguments_.size() - 1, types::kIntArgument};
    indexes_arguments_[std::string(1, short_name)] = {int_arguments_.size() - 1, types::kIntArgument};
    return int_arguments_.back();
}

int ArgParser::GetIntValue(const char* name, int64_t index /*=0*/) const{
    return int_arguments_[indexes_arguments_.at(std::string(name)).first].GetValue(index);
}

Argument<bool>& ArgParser::AddFlag(const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<bool>();
    new_argument.AddValue(false);
    new_argument.AddHelpInformation("--" + std::string(full_name) + ", " + description);
    flag_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {flag_arguments_.size() - 1, types::kFlagArgument};

    return flag_arguments_.back();
}

Argument<bool>& ArgParser::AddFlag(char short_name, const char* full_name, const char* description /*=""*/) {
    Argument new_argument = Argument<bool>();
    new_argument.AddValue(false);
    new_argument.AddHelpInformation("-" + std::string(1, short_name) + ", --" + std::string(full_name) + ", " + description);
    flag_arguments_.push_back(new_argument);
    indexes_arguments_[std::string(full_name)] = {flag_arguments_.size() - 1, types::kFlagArgument};
    indexes_arguments_[std::string(1,short_name)] = {flag_arguments_.size() - 1, types::kFlagArgument};
    return flag_arguments_.back();
}

bool ArgParser::GetFlag(const char* name, int64_t index /*=0*/) const{
    return flag_arguments_[indexes_arguments_.at(std::string(name)).first].GetValue(index);
}

void ArgParser::AddHelp(const char* full_name, const char* description /*=""*/) {
    AddStringArgument(full_name);
    string_arguments_[indexes_arguments_[std::string(full_name)].first].AddValue(std::string(description));
    string_arguments_[indexes_arguments_[std::string(full_name)].first].SetHelp();
    help_description_ += std::string(description) + '\n' + '\n';

}

void ArgParser::AddHelp(char short_name, const char* full_name, const char* description /*=""*/) {
    AddStringArgument(short_name, full_name);
    string_arguments_[indexes_arguments_[full_name].first].AddValue(std::string(description));
    string_arguments_[indexes_arguments_[full_name].first].SetHelp();
    help_description_ += std::string(description) + '\n' + '\n';
}


bool ArgParser::Help() const{
    return was_called_help;
}

std::string ArgParser::HelpDescription() const {
    std::string help_description = help_description_;

    for (uint64_t i = 0; i < string_arguments_.size(); ++i) {
        if (string_arguments_[i].CheckIsHelp()) continue;
        help_description += string_arguments_[i].GetHelpDescription() + "\n";
    }
    for (uint64_t i = 0; i < int_arguments_.size(); ++i) {
        if (int_arguments_[i].CheckIsHelp()) continue;
        help_description += int_arguments_[i].GetHelpDescription() + "\n";
    }
    for (uint64_t i = 0; i < flag_arguments_.size(); ++i) {
        if (flag_arguments_[i].CheckIsHelp()) continue;
        help_description += flag_arguments_[i].GetHelpDescription() + "\n";
    }

    return help_description;
}

int64_t ArgParser::StringArgumentGetNumberElements(const char* name) {
    return string_arguments_[indexes_arguments_.at(std::string(name)).first].GetNumberArgumnets();
}

void ArgParser::FindPositional(int64_t& pos_string, int64_t& pos_int, int64_t& pos_flag) {
    for (uint64_t i = 0; i < string_arguments_.size(); ++i) {
        if (string_arguments_[i].CheckIsPostional()) {
            pos_string  = i;
            break;
        }
    }
    
    for (uint64_t i = 0; i < int_arguments_.size(); ++i) {
        if (int_arguments_[i].CheckIsPostional()) {
            pos_int  = i;
            break;
        }
    }

    for (uint64_t i = 0; i < flag_arguments_.size(); ++i) {
        if (flag_arguments_[i].CheckIsPostional()) {
            pos_flag  = i;
            break;
        }
    }
}
bool ArgParser::CheckNotInitializedArgs() {
    if (!std::all_of(string_arguments_.begin(), string_arguments_.end(), [](Argument<std::string> arg) {return arg.CheckIsInitialized();})) return false;
    if (!std::all_of(int_arguments_.begin(), int_arguments_.end(), [](Argument<int> arg) {return arg.CheckIsInitialized();})) return false;
    //if (!std::all_of(flag_arguments_.begin(), flag_arguments_.end(), [](Argument<bool> arg) {return arg.CheckIsInitialized();})) return false;

    return true;
}

bool ArgParser::LongNameHandle(const std::string& argument) {
    uint64_t eq_position = argument.find('=');
    std::string buff;

    if (eq_position == std::string::npos) {
        buff = argument.substr(2, argument.size() - 2);
    } else {
        buff = argument.substr(2, eq_position - 2);
    }

    if (indexes_arguments_.find(buff) == indexes_arguments_.end()) {
        return false;
    }

    switch (indexes_arguments_[buff].second) {
        case types::kStringArgument:
            if (string_arguments_[indexes_arguments_[buff].first].CheckIsHelp()) {
                was_called_help = true;
            }

            string_arguments_[indexes_arguments_[buff].first].AddValue(argument.substr(eq_position + 1, argument.size() - (eq_position + 1)));
            break;
        case types::kIntArgument:
            int_arguments_[indexes_arguments_[buff].first].AddValue(stoi(argument.substr(eq_position + 1, argument.size() - (eq_position + 1)), nullptr, 10));
            break;
        case types::kFlagArgument:
            flag_arguments_[indexes_arguments_[buff].first].AddValue(true);
            break;
    }

    return true;
}

bool ArgParser::ShortNameHandle(const std::string& argument) {
    bool is_flag = false;
    std::string buff;
    for (uint64_t j = 1; j < argument.size(); ++j) {
        buff = argument[j];

        if (indexes_arguments_.find(buff) == indexes_arguments_.end()) {
            return false;
        }

        switch (indexes_arguments_[buff].second) {
            case types::kStringArgument:
                if (string_arguments_[indexes_arguments_[buff].first].CheckIsHelp()) {
                    was_called_help = true;
                }

                string_arguments_[indexes_arguments_[buff].first].AddValue(argument.substr(j + 2, argument.size() - (j + 2)));
                break;
            case types::kIntArgument:
                int_arguments_[indexes_arguments_[buff].first].AddValue(stoi(argument.substr(j + 2, argument.size() - (j + 2)), nullptr, 10));
                break;
            case types::kFlagArgument:
                flag_arguments_[indexes_arguments_[buff].first].AddValue(true);
                is_flag = true;
        }
        if (j == argument.size() - 1 || !is_flag) break;
    }
    return true;
}

bool ArgParser::Parse(const std::vector<std::string>& arguments) {
    int64_t pos_string = -1;
    int64_t pos_int = -1;
    int64_t pos_flag = -1;
    FindPositional(pos_string, pos_int, pos_flag);

    for (uint64_t i = 1; i < arguments.size(); ++i) {
        if (arguments[i][0] == '-' && arguments[i][1] == '-') {
            if (!LongNameHandle(arguments[i])) return false;
        } else if (arguments[i][0] == '-') {
            if (!ShortNameHandle(arguments[i])) return false;
        } else {
            if (pos_string != -1) {
                string_arguments_[pos_string].AddValue(arguments[i]);
            } else if (pos_int != -1) {
                int_arguments_[pos_int].AddValue(stoi(arguments[i], nullptr, 10));
            } else if (pos_flag != -1){
                flag_arguments_[pos_flag].AddValue(true);
            } else {
                return false;
            }
        }
        if (was_called_help) return true; 
    }
    return CheckNotInitializedArgs();
}