#include "archiver.h"

std::ifstream::pos_type GetFileSize(std::string filename) {
    std::ifstream in(filename, std::ios::ate | std::ifstream::binary);
    if (!in.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    return in.tellg();
}

void ReadCodeWord(std::vector<bool>& codeword, uint8_t& byte_in, int8_t& cur_bit_in, std::istream& in, std::size_t& bytes_read, std::size_t filesize, bool coded) {
    for (std::size_t i = 1; i < codeword.size(); ++i) {
        codeword[i] = 0;
    }

    for (std::size_t i = 0; i < codeword.size(); ++i) {
        if (cur_bit_in == -1) {
            if (bytes_read > filesize) {
                break;
            }
            ++bytes_read;
            in.read(reinterpret_cast<char*>(&byte_in), 1);
            cur_bit_in = kBitsInByte - 1;
        }

        if (coded) {
            if ((byte_in & (1 << cur_bit_in)) > 0) {
                codeword[i] = 1;
            } else {
                codeword[i] = 0;
            }
            --cur_bit_in;
            continue;
        }

        if ((i & (i - 1)) != 0 && i > 0) {
            if ((byte_in & (1 << cur_bit_in)) > 0) {
                codeword[i] = 1;
            } else {
                codeword[i] = 0;
            }
            --cur_bit_in;
        }
    }
}

void WriteCodeWord(std::vector<bool>& codeword, std::ostream& out, int8_t& cur_bit_out, uint8_t& byte_out) {
    for (std::size_t i = 0; i < codeword.size(); ++i) {
        if (cur_bit_out == -1) {
            out.write(reinterpret_cast<const char*>(&byte_out), 1);
            byte_out = 0;
            cur_bit_out =  kBitsInByte - 1;
        }

        byte_out |= (codeword[i] << cur_bit_out);
        --cur_bit_out;

        if (cur_bit_out == -1) {
            out.write(reinterpret_cast<const char*>(&byte_out), 1);
            byte_out = 0;
            cur_bit_out =  kBitsInByte - 1;
        }
    }
}

void Encode(std::vector<bool>& codeword, std::size_t number_of_redundant_bits) {
    for (std::size_t i = 1; i < codeword.size(); ++i) {
        for (std::size_t j = 0; j < number_of_redundant_bits; ++j) {
            if (((((i - (1 << j)) / (1 << j)) % 2) == 0) && (i > static_cast<std::size_t>((1 << j)))) {
                codeword[1 << j] = static_cast<int64_t>(codeword[1 << j]) ^ static_cast<int64_t>(codeword[i]); 
            }
        }
    }

    for (std::size_t i = 0; i < codeword.size(); ++i) {
        codeword[0] = static_cast<int64_t>(codeword[0]) ^ static_cast<int64_t>(codeword[i]);
    }
}

void Decode(std::vector<bool>& coded_codeword, std::vector<bool>& decoded_codeword, std::size_t number_of_redundant_bits) {
    std::vector<bool> check_control_bits(number_of_redundant_bits + 1);
    for (std::size_t i = 0; i < number_of_redundant_bits + 1; ++i) {
        check_control_bits[i] = 0;
    }
    for (std::size_t i = 0; i < decoded_codeword.size(); ++i) {
        decoded_codeword[i] = 0;
    }

    for (std::size_t i = 1; i < coded_codeword.size(); ++i) {
        for (std::size_t j = 0; j < number_of_redundant_bits; ++j) {
            if (((((i - (1 << j)) / (1 << j)) % 2) == 0) && (i > static_cast<std::size_t>((1 << j)))) {
                coded_codeword[1 << j] = static_cast<int64_t>(coded_codeword[1 << j]) ^ static_cast<int64_t>(coded_codeword[i]);
            }
        }
    }

    for (std::size_t i = 1; i < coded_codeword.size(); ++i) {
        coded_codeword[0] = static_cast<int64_t>(coded_codeword[0]) ^ static_cast<int64_t>(coded_codeword[i]);
    }

    std::size_t error_bit = 0;

    for (std::size_t i = 1; i < check_control_bits.size(); ++i) {
        if (check_control_bits[i] != coded_codeword[(1 << i)]) {
            error_bit += (1 << i);
        }
    }

    if (error_bit > 0) {
        if (check_control_bits[0] != 0) {
            std::cerr << "Double error in file" << std::endl;
            std::exit(EXIT_SUCCESS);
        } 
        coded_codeword[error_bit] = coded_codeword[error_bit] ^ 1;
    }

    std::size_t cur_bit = 0;

    for (std::size_t i = 1; i < coded_codeword.size(); ++i) {
        if ((i & (i - 1)) != 0) {
            decoded_codeword[cur_bit] = coded_codeword[i];
            ++cur_bit;
        }
    }
}

void ReadAndDecodeFile(std::ifstream& in, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::string& filename, std::size_t coded_filesize) {
    std::ofstream out(filename, std::ofstream::binary);
    if (!out.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }

    std::size_t bytes_read = 1;
    int8_t cur_bit_in = kBitsInByte - 1;
    int8_t cur_bit_out = kBitsInByte - 1;
    uint8_t byte_in;
    uint8_t byte_out = 0;

    in.read(reinterpret_cast<char*>(&byte_in), 1);
    std::vector<bool> coded_codeword(kInfoLength + number_of_redundant_bits + 1);
    std::vector<bool> decoded_codeword(kInfoLength);

    while (bytes_read < coded_filesize) {
        ReadCodeWord(coded_codeword, byte_in, cur_bit_in, in, bytes_read, coded_filesize, true);
        Decode(coded_codeword, decoded_codeword, number_of_redundant_bits);
        WriteCodeWord(decoded_codeword, out, cur_bit_out, byte_out);
    }
}

void ReadAndWriteCodedFile(std::ofstream& out, std::string& filename, std::size_t number_of_redundant_bits, std::size_t filesize, const int64_t kInfoLength) {
    std::ifstream in(filename, std::ifstream::binary);
    if (!in.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    std::size_t bytes_read = 1;
    int8_t cur_bit_in = kBitsInByte - 1;
    int8_t cur_bit_out = kBitsInByte - 1;
    uint8_t byte_in;
    uint8_t byte_out = 0;
    in.read(reinterpret_cast<char*>(&byte_in), 1);
    std::vector<bool> codeword(kInfoLength + number_of_redundant_bits + 1);
    
    while (bytes_read < filesize) {
        ReadCodeWord(codeword, byte_in, cur_bit_in, in, bytes_read, filesize, false);
        Encode(codeword, number_of_redundant_bits);
        WriteCodeWord(codeword, out, cur_bit_out, byte_out);
    }

    if (cur_bit_in != -1) {
        ReadCodeWord(codeword, byte_in, cur_bit_in, in, bytes_read, filesize + 1, false);
        Encode(codeword, number_of_redundant_bits);
        WriteCodeWord(codeword, out, cur_bit_out, byte_out);
    }

    if (cur_bit_out != (kBitsInByte - 1)) {
        out.write(reinterpret_cast<const char*>(&byte_out), 1);
        byte_out = 0;
        cur_bit_out =  kBitsInByte - 1;
    }
}

void ReadDecodeAndWriteHeader(std::ifstream& in, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::size_t& filename_size, std::string& filename, std::size_t& coded_filesize, std::size_t& filesize) {
    std::stringstream header;

    std::size_t bytes_read = 1;
    int8_t cur_bit_in = kBitsInByte - 1;
    int8_t cur_bit_out = kBitsInByte - 1;
    uint8_t byte_in;
    uint8_t byte_out = 0;
    std::size_t coded_filenamesize_length = (((kLongLongSize * kBitsInByte + kInfoLength - 1) / kInfoLength) * (kInfoLength + number_of_redundant_bits + 1) + kBitsInByte - 1) / kBitsInByte;
    in.read(reinterpret_cast<char*>(&byte_in), 1);
    std::vector<bool> coded_codeword(kInfoLength + number_of_redundant_bits + 1);
    std::vector<bool> decoded_codeword(kInfoLength);

    while (bytes_read < coded_filenamesize_length) {
        ReadCodeWord(coded_codeword, byte_in, cur_bit_in, in, bytes_read, coded_filenamesize_length, true);
        Decode(coded_codeword, decoded_codeword, number_of_redundant_bits);
        WriteCodeWord(decoded_codeword, header, cur_bit_out, byte_out);
    }

    header.read(reinterpret_cast<char*>(&filename_size), kLongLongSize);
    std::size_t coded_header_size = ((((filename_size + 3 * kLongLongSize) * kBitsInByte + kInfoLength - 1) / kInfoLength) * (kInfoLength + number_of_redundant_bits + 1) + kBitsInByte - 1) / kBitsInByte;

    while (bytes_read < coded_header_size) {
        ReadCodeWord(coded_codeword, byte_in, cur_bit_in, in, bytes_read, coded_header_size, true);
        Decode(coded_codeword, decoded_codeword, number_of_redundant_bits);
        WriteCodeWord(decoded_codeword, header, cur_bit_out, byte_out);
    }

    filename = std::string(filename_size, '\0');
    header.read(&filename[0], filename_size);
    header.read(reinterpret_cast<char*>(&coded_filesize), kLongLongSize);
    header.read(reinterpret_cast<char*>(&filesize), kLongLongSize);
}

void ReadAndWriteCodedHeader(std::ofstream& out, const int64_t kInfoLength, std::size_t number_of_redundant_bits, std::size_t filename_size, std::string& filename, std::size_t coded_filesize, std::size_t filesize) {
    std::stringstream header;
    header.write(reinterpret_cast<char*>(&filename_size), kLongLongSize);
    header.write(&filename[0], filename_size);
    header.write(reinterpret_cast<char*>(&coded_filesize), kLongLongSize);
    header.write(reinterpret_cast<char*>(&filesize), kLongLongSize);
    std::size_t bytes_read = 1;
    int8_t cur_bit_in = kBitsInByte - 1;
    int8_t cur_bit_out = kBitsInByte - 1;
    uint8_t byte_in;
    uint8_t byte_out = 0;
    header.read(reinterpret_cast<char*>(&byte_in), 1);
    std::vector<bool> codeword(kInfoLength + number_of_redundant_bits + 1);

    while (bytes_read < (3 * kLongLongSize + filename_size)) {
        ReadCodeWord(codeword, byte_in, cur_bit_in, header, bytes_read, 3 * kLongLongSize + filename_size, false);
        Encode(codeword, number_of_redundant_bits);
        WriteCodeWord(codeword, out, cur_bit_out, byte_out);
    }
    
    if (cur_bit_in != -1) {
        ReadCodeWord(codeword, byte_in, cur_bit_in, header, bytes_read, 3 * kLongLongSize + filename_size + 1, false);
        Encode(codeword, number_of_redundant_bits);
        WriteCodeWord(codeword, out, cur_bit_out, byte_out);
    }

    if (cur_bit_out != (kBitsInByte - 1)) {
        out.write(reinterpret_cast<const char*>(&byte_out), 1);
        byte_out = 0;
        cur_bit_out =  kBitsInByte - 1;
    }
}



namespace HamArc {

void CreateArchive(ArgumentParser::ArgParser& parser) {
    std::ofstream archive(parser.GetStringValue("file"), std::ofstream::binary);
    if (!archive.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    const int64_t kInfoLength = parser.GetIntValue("length");
    std::size_t number_of_files = parser.StringArgumentGetNumberElements("filename");
    std::string filename;
    std::size_t filename_size;
    int64_t filesize;
    std::size_t number_of_redundant_bits;
    std::size_t coded_filesize;

    for (std::size_t i = 0; i < number_of_files; ++i) {
        filename = parser.GetStringValue("filename", i);
        filename_size = parser.GetStringValue("filename", i).size();
        filesize = GetFileSize(parser.GetStringValue("filename", i));
        number_of_redundant_bits = 1;

        while ((1 << number_of_redundant_bits) < (kInfoLength + number_of_redundant_bits + 1)){
            ++number_of_redundant_bits;
        }
        coded_filesize = (((filesize * kBitsInByte + kInfoLength - 1) / kInfoLength) * (kInfoLength + number_of_redundant_bits + 1) + kBitsInByte - 1) / kBitsInByte;

        ReadAndWriteCodedHeader(archive, kInfoLength, number_of_redundant_bits, filename_size, filename, coded_filesize, filesize);

        ReadAndWriteCodedFile(archive, filename, number_of_redundant_bits, filesize, kInfoLength);
    }
}

void AppendFile(ArgumentParser::ArgParser& parser) {
    std::ofstream archive(parser.GetStringValue("file"), std::ofstream::binary | std::ios::app);
    if (!archive.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    const std::size_t kInfoLength = parser.GetIntValue("length");
    std::string filename;
    std::size_t filename_size;
    int64_t filesize;
    std::size_t number_of_redundant_bits;
    std::size_t coded_filesize;

    filename = parser.GetStringValue("filename", 0);
    filename_size = parser.GetStringValue("filename", 0).size();
    filesize = GetFileSize(parser.GetStringValue("filename", 0));
    number_of_redundant_bits = 1;

    while ((1 << number_of_redundant_bits) < (kInfoLength + number_of_redundant_bits + 1)){
        ++number_of_redundant_bits;
    }

    coded_filesize = (((filesize * kBitsInByte + kInfoLength - 1) / kInfoLength) * (kInfoLength + number_of_redundant_bits + 1) + kBitsInByte - 1) / kBitsInByte;

    ReadAndWriteCodedHeader(archive, kInfoLength, number_of_redundant_bits, filename_size, filename, coded_filesize, filesize);

    ReadAndWriteCodedFile(archive, filename, number_of_redundant_bits, filesize, kInfoLength);
}

void ShowListOfFiles(ArgumentParser::ArgParser& parser) {
    std::ifstream archive_in(parser.GetStringValue("file"), std::ifstream::binary);
    if (!archive_in.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    std::size_t archive_size = GetFileSize(parser.GetStringValue("file"));
    std::size_t kInfoLength = parser.GetIntValue("length");
    std::size_t number_of_redundant_bits = 1;
    std::string filename;
    std::size_t filename_size;
    std::size_t coded_filesize;
    std::size_t filesize;

    while ((1 << number_of_redundant_bits) < (kInfoLength + number_of_redundant_bits + 1)){
        ++number_of_redundant_bits;
    }

    while (static_cast<std::size_t>(archive_in.tellg()) < archive_size) {
        ReadDecodeAndWriteHeader(archive_in, kInfoLength, number_of_redundant_bits, filename_size, filename, coded_filesize, filesize);
        std::cout << filename << ' ';
        archive_in.seekg(coded_filesize, std::ios::cur);
    }

    std::cout << std::endl;
}

void ExtractFile(ArgumentParser::ArgParser& parser) {
    std::ifstream archive(parser.GetStringValue("file"), std::ifstream::binary);
    if (!archive.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }

    std::size_t number_of_files = parser.StringArgumentGetNumberElements("filename");
    std::size_t archive_size = GetFileSize(parser.GetStringValue("file"));
    const std::size_t kInfoLength = parser.GetIntValue("length");
    std::string extracting_filename;
    std::size_t filename_size;
    std::size_t filesize;
    std::size_t count_files = 0;
    std::size_t number_of_redundant_bits = 0;
    std::size_t coded_filesize;
    std::vector<std::string> filenames(number_of_files);

    for (std::size_t i = 0; i < number_of_files; ++i) {
        filenames[i] = parser.GetStringValue("filename", i);
    }

    while ((1 << number_of_redundant_bits) < (kInfoLength + number_of_redundant_bits + 1)){
        ++number_of_redundant_bits;
    }

    while (static_cast<std::size_t>(archive.tellg()) < archive_size) {
        if (number_of_files != 0 && count_files > number_of_files) {
            break;
        }

        ReadDecodeAndWriteHeader(archive, kInfoLength, number_of_redundant_bits, filename_size, extracting_filename, coded_filesize, filesize);

        if (number_of_files == 0 || (std::find(filenames.begin(), filenames.end(), extracting_filename) != filenames.end())) {
            ReadAndDecodeFile(archive, kInfoLength, number_of_redundant_bits, extracting_filename, coded_filesize);
            ++count_files;
        } else {
            archive.seekg(coded_filesize, std::ios::cur);
        }
    }
}

void DeleteFile(ArgumentParser::ArgParser& parser) {
    std::ifstream archive_in(parser.GetStringValue("file"), std::ifstream::binary);
    if (!archive_in.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }
    std::ofstream temp_out("temporary.haf", std::ofstream::binary);
    std::size_t archive_size = GetFileSize(parser.GetStringValue("file"));
    const std::size_t kInfoLength = parser.GetIntValue("length");
    std::string filename;
    std::string deleting_filename = parser.GetStringValue("filename", 0);
    std::size_t filename_size;
    std::size_t filesize;
    std::size_t number_of_redundant_bits = 1;
    std::size_t coded_filesize;
    uint8_t buffer;
    std::size_t coded_header_size;
    
    while ((1 << number_of_redundant_bits) < (kInfoLength + number_of_redundant_bits + 1)){
        ++number_of_redundant_bits;
    }

    while (static_cast<std::size_t>(archive_in.tellg()) < archive_size) {
        ReadDecodeAndWriteHeader(archive_in, kInfoLength, number_of_redundant_bits, filename_size, filename, coded_filesize, filesize);
        if (filename != deleting_filename) {
            coded_header_size = ((((filename_size + 3 * kLongLongSize) * kBitsInByte + kInfoLength - 1) / kInfoLength) * (kInfoLength + number_of_redundant_bits + 1) + kBitsInByte - 1) / kBitsInByte;
            archive_in.seekg(-coded_header_size, std::ios::cur);
            for (std::size_t i = 0; i < (coded_header_size + coded_filesize); ++i) {
                archive_in.read(reinterpret_cast<char*>(&buffer), 1);
                temp_out.write(reinterpret_cast<char*>(&buffer), 1);
            }
        } else {
            archive_in.seekg(coded_filesize, std::ios::cur);
        }
    }

    archive_in.close();
    temp_out.close();
    std::ofstream archive_out(parser.GetStringValue("file"), std::ofstream::binary);
    std::ifstream temp_in("temporary.haf", std::ifstream::binary);
    archive_size = GetFileSize("temporary.haf");

    while (static_cast<std::size_t>(temp_in.tellg()) < archive_size) {
        temp_in.read(reinterpret_cast<char*>(&buffer), 1);
        archive_out.write(reinterpret_cast<char*>(&buffer), 1);
    }

    std::remove("temporary.haf");
} 

void ConcatenateArchives(ArgumentParser::ArgParser& parser) {
    std::ofstream archive_out(parser.GetStringValue("file"), std::ofstream::binary);
    if (!archive_out.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }

    if (parser.StringArgumentGetNumberElements("filename") != 2) {
        std::cerr << "Concantenate must have take 2 files\n";
        std::exit(EXIT_FAILURE);
    }

    std::ifstream archive_in_1(parser.GetStringValue("filename", 0), std::ifstream::binary);
    if (!archive_in_1.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }

    std::ifstream archive_in_2(parser.GetStringValue("filename", 1), std::ifstream::binary);
    if (!archive_in_2.is_open()) {
        std::cerr << "Incorrect file";
        std::exit(EXIT_FAILURE);
    }

    uint8_t buffer;
    std::size_t archive_size_1 = GetFileSize(parser.GetStringValue("filename", 0));
    std::size_t archive_size_2 = GetFileSize(parser.GetStringValue("filename", 1));

    for (std::size_t i = 0; i < archive_size_1; ++i) {
        archive_in_1.read(reinterpret_cast<char*>(&buffer), 1);
        archive_out.write(reinterpret_cast<char*>(&buffer), 1);
    }

    for (std::size_t i = 0; i < archive_size_2; ++i) {
        archive_in_2.read(reinterpret_cast<char*>(&buffer), 1);
        archive_out.write(reinterpret_cast<char*>(&buffer), 1);
    }
}

}